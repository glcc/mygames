//
//  ViewController+CoreData.swift
//  MyGames
//
//  Created by Gerson Costa on 25/11/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit
import CoreData

extension UIViewController {
    
    var context: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
}
