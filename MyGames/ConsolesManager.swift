//
//  ConsolesManager.swift
//  MyGames
//
//  Created by Gerson Costa on 26/11/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit
import CoreData

class ConsolesManager {
    static let shared = ConsolesManager()
    var consoles: [Console] = []
    
    func loadConsoles(with context: NSManagedObjectContext) {
        let fetchRequest: NSFetchRequest<Console> = Console.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            consoles = try context.fetch(fetchRequest)
        } catch {
            print("Error loading Consoles from CoreData. Error: \(error.localizedDescription)")
        }
    }
    
    func deleteConsole(index: Int, context: NSManagedObjectContext) {
        let console = consoles[index]
        context.delete(console)
        do {
            try context.save()
        } catch {
            print("Error removing console from CoreData. Error: \(error.localizedDescription)")
        }
    }
    
    private init() {
        
    }
}
